---
name: "Kathara"
avathar: "https://github.com/katharaa.png?size=200"
designation: "Technical Advisor"
url: "http://github.com/katharaa"
dept: "CSE"
email: "katharasasikumar007@gmail.com"
phone: "+91 999 560 3649"
skills: "Python Developer"
---
