---
path: /events/opentalks-ep1/
date: '2024-08-06'
datestring: '06 August 2024'
author: 'noelty'
title: 'Open Talks: Opensource How&Why'
cover: './opentalks-ep1.jpg'
name: 'Noel Tom Santhosh'
---

![Poster](./opentalks-ep1.jpg)

## Open Talks Episode 1: Fundamentals of FOSS

Join us for **Episode 1 of Open Talks** presented by **FOSSNSS**, an enlightening orientation session on the fundamentals and benefits of **Free and Open Source Software (FOSS)**.

Open source has significantly impacted technology through innovations such as **Linux**, **Apache**, **Firefox**, **LibreOffice**, and **Git**. Discover what FOSS is, why it matters, and how you can get involved. We'll cover the principles, community practices, and opportunities within the FOSS ecosystem.

### Session Details:

-   **Speaker🎙️**: Suneesh S
-   **Date🗓️**: 6th August 2024
-   **Time⏳**: 4:30 PM
-   **Venue📍**: CSE Seminar Hall

Don't miss this chance to explore the world of open source and its impact!

---
