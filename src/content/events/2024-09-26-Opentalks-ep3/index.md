---
path: /events/opentalks-ep3/
date: '2024-09-26'
datestring: '26 September 2024'
author: 'noelty'
title: 'Git ready for Hacktoberfest'
cover: './opentalks-ep3.jpg'
name: 'Noel Tom Santhosh'
---

![Poster](./opentalks-ep3.jpg)

## Intro to Git and Hacktoberfest

Join us for **Intro to Git and Hacktoberfest!** Presented by **FOSSNSS**🚀

Discover the basics of **Git** 🖥️—a free, open-source version control system that makes managing projects and collaboration easier. Whether you’re new to coding or just starting with version control, this session will introduce you to Git in a simple and practical way.

You’ll also get a chance to explore **Hacktoberfest** 🧑‍💻🎉, a fun, month-long event where you can contribute to open-source projects and become part of a global community 🌍. No prior experience needed—just enthusiasm to learn and get involved!

### Session Details:

-   **Speakers 🎙️**: Mohammed Shabeeb, Ashish B
-   **Date🗓️**: 26th September 2024
-   **Time⏳**: 4:30 PM
-   **Venue📍**: CSE Seminar Hall

Come along to learn, contribute, and kickstart your journey into the world of open source! ✨
